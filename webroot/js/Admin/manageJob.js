$(function() {
	$('.switchJobs').on('switchChange.bootstrapSwitch', function (event, state) {
	    var id = $(this).data('id');
	    var element = $(this).closest("tr").find('.label-status');
	    changeStatus(id, state, element);
	});
	$('.delete-list').on('click', function(){
		var element = $(this);
		var id = $(this).attr('data-id');
		$('#heading').html('Delete job');
		$('#innerText').html('Are you sure you want to delete this job?');
		$('#confirmDelete').modal('show');
		$('#confirm').unbind('click').click(function() {
			deleteList(id, element);
		});
	});

	$('#clearForm').on('click', function (event) {
		event.preventDefault();
		$('#jobForm').trigger('reset');
	})
});

function changeStatus(id, state, element)
{
	$.ajax({
		method: 'GET',
		url: window.url+ 'api/Admins/changeJobStatus.json?id='+id+'&&state='+state,
		success: function(data) {
			$('#messageModal').modal('show');
			if (data.response) {
				if (state) {
					element.removeClass('label-danger');
					element.addClass('label-success');
					element.html('open');
				} else {
					element.removeClass('label-success');
					element.addClass('label-danger');
					element.html('closed');
				}
			}
			$('#modalText').html(data.response.message);
		},
		error: function(error) {
			$('#messageModal').modal('show');
			$('#modalText').html(error);
		}
	});
}

///Function to Delete the List
function deleteList(id, element){
	$('#confirmDelete').modal('hide');
	$.ajax({
		method: 'GET',
		url: window.url+ 'api/Admins/deleteJob.json?id='+id,
		success: function(data) {
			$('#messageModal').modal('show');
			if (data.response.status) {
				$('#modalHead').html('Message!');
				$('#modalText').html(data.response.message);
				element.closest("tr").remove();
				//By default tr length is 1 for th body
				if ($('.table tr').length == 1) {
					location.reload();
				}
			 } else {
			 	$('#modalHead').html('Message!');
				$('#modalText').html(data.response.message);
			}	
		},
		complete: function(){
         	$("#loader").hide(); //hide loading here
        },
		error: function(error) {
			$("#loader").hide();
			$('#messageModal').modal('show');
			$('#modalHead').html('Housekeeper Removed!');
			$('#modalText').html(error);
		}
	});
}