(function(){
	var validatePage = function (element) {  
	    $(element).validate({
	        rules: {
	            'meta_title' :{
	                required: true,
	                minlength: 3
	            },
	            'page_name' :{
	                required: true,
	                minlength: 3
	            },
	            'page_content' :{
	                required: true,
	                minlength: 10,
	                number:true
	            }
	        },
	        messages: {       
	            
	        },
	        errorPlacement: function(error, element) {
                error.insertAfter(element);
	        }
	        
	    });
	};
	validatePage('form'); 
})();