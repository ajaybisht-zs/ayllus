$(function() {
	$('.switchCheckbox').on('switchChange.bootstrapSwitch', function (event, state) {
	    var id = $(this).data('id');
	    changeStatus(id, state);
	});
	$('.delete-list').on('click', function(){
		var element = $(this);
		var id = $(this).attr('data-id');
		$('#heading').html('Delete Candidate');
		$('#innerText').html('Are you sure you want to delete this Candidate?');
		$('#confirmDelete').modal('show');
		$('#confirm').unbind('click').click(function() {
			deleteList(id, element);
		});
	});

	$('#clearForm').on('click', function (event) {
		event.preventDefault();
		$('#companiesForm').trigger('reset');
	})
});

function changeStatus(id, state)
{
	$.ajax({
		method: 'GET',
		url: window.url+ 'api/Admins/active.json?id='+id+'&&state='+state,
		success: function(data) {
			$('#messageModal').modal('show');
			$('#modalText').html(data.response.message);
		},
		error: function(error) {
			$('#messageModal').modal('show');
			$('#modalText').html(error);
		}
	});
}

//Function to Delete the List
function deleteList(id, element){
	$('#confirmDelete').modal('hide');
	$.ajax({
		method: 'GET',
		url: window.url+ 'api/Admins/deleteCandidate.json?id='+id,
		success: function(data) {
			$('#messageModal').modal('show');
			if (data.response.status) {
				$('#modalHead').html('Company Removed!');
				$('#modalText').html(data.response.message);
				element.parent().parent().remove();
				//By default tr length is 1 for th body
				if ($('.table tr').length == 1) {
					location.reload();
				}
			 } else {
			 	$('#modalHead').html('Company Removed!');
				$('#modalText').html(data.response.message);
			}	
		},
		complete: function(){
         	$("#loader").hide(); //hide loading here
        },
		error: function(error) {
			$("#loader").hide();
			$('#messageModal').modal('show');
			$('#modalHead').html('Housekeeper Removed!');
			$('#modalText').html(error);
		}
	});
}