<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Mailer\Email;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']

                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'index',
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Method _setValidationErrorForInvoice to set validation errors for invoice
     *
     * @param $errors array containing the error list
     * @return $str string containg the list of errors as string
     */
    protected function _setValidationError($errors = array()) {
        $allErrors = Hash::flatten($errors);
        $allErrors = array_unique($allErrors);
        $str = null;
        if (!empty($allErrors)) {
            $str = '<ul>';
            foreach ($allErrors as $key => $val):
                $str.= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }

    /**
     * Method _sendEmailMessage to send email from website
     *
     * @param $to string contain the receiver's email address
     * @param $email_body string the body of the email to send
     * @param $subject string subject of the email
     * @param $attachments array containg the array of attachments if any
     * @return bool
     */
    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $attachments = [])
    {
        $email = new Email('default');
        $email->from(['hr@ayllus.com'])
            ->to($to)
            ->emailFormat('html')
            ->subject($subject);
        if (!empty($attachments)) {
            $email->attachments($attachments);
        }
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

}
