<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class AgentsController extends AppController
{
	public $limit = 10;
    public $paginate = [
        'limit' => 10
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->Auth->allow('index');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    	$this->viewBuilder()->layout('backend/admin');
    	        try {
            if (!isset($this->request->query['search']) ) {
                $this->request->query['search'] = null;
            }
            $usersTable = TableRegistry::get('Users');
            $queryToGetSubAdmins = $usersTable->find()
                                            ->where(function ($exp) {
                                                $orConditions = $exp->or_(['UserProfiles.first_name LIKE' => '%' . $this->request->query['search'] . '%'])
                                                                    ->like('UserProfiles.last_name', '%' . $this->request->query['search'] . '%');
                                                                    
                                                return $exp
                                                    ->add($orConditions)
                                                    ->eq('Users.user_role_id',2);
                                            })
                                            ->contain([
                                                'UserProfiles'
                                            ]);
            $this->paginate = [
                    'sortWhitelist' => [
                        'UserProfiles.first_name', 'UserProfiles.last_name'
                    ],
                    'limit' => 25
                ];
            $users = $this->paginate($queryToGetSubAdmins);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            return $this->redirect(DS . 'admin' . DS . $this->request->params['controller'] . DS . $this->request->params['action'] . '?page=' . $this->request->getParam('paging')['Users']['page']);
        }         
            
      $this->set(compact('users'));
    }

    public function approve($id) {
    	$this->loadModel('Users');
    	$user = $this->Users->find()
                            ->where([
                                'Users.id' => base64_decode($id)
                            ]);

        if ($user->isEmpty()) {
            $this->Flash->error(__('User does not exist'));
            return $this->redirect($this->referer());
        }
        $user = $user->first();
        $user->status = 1;
        if ($this->Users->save($user)) {
        	$this->loadModel('EmailTemplates');
            $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>1])
            		-> first();
              
            $temp['mail_body'] = str_replace(
                    array('#NAME'),
                    array(
                        $user['first_name'].' '.$user['last_name']
                    ), 
                $temp['mail_body']
            );
            $this->_sendEmailMessage('damanjit.kaur@ucodesoft.com', $temp['mail_body'], $temp['subject']);
            $this->Flash->success(__('Application has been approved'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Some errors occurred while approving user. Please try again.'));
        return $this->redirect($this->referer());
    }

    public function disapprove($id) {
    	$this->loadModel('Users');
    	$user = $this->Users->find()
                            ->where([
                                'Users.id' => base64_decode($id)
                            ]);

        if ($user->isEmpty()) {
            $this->Flash->error(__('User does not exist'));
            return $this->redirect($this->referer());
        }
        $user = $user->first();
        $user->status = 2;
        if ($this->Users->save($user)) {
        	$this->loadModel('EmailTemplates');
            $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>3])
            		-> first();
              
            $temp['mail_body'] = str_replace(
                    array('#NAME'),
                    array(
                        $user['first_name'].' '.$user['last_name']
                    ), 
                $temp['mail_body']
            );
            $this->_sendEmailMessage('damanjit.kaur@ucodesoft.com', $temp['mail_body'], $temp['subject']);
            $this->Flash->success(__('Application has been disapproved'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Some errors occurred while approving user. Please try again.'));
        return $this->redirect($this->referer());
    }
}