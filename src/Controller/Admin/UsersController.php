<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Auth\DefaultPasswordHasher;
/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $limit = 10;
    public $paginate = [
        'limit' => 10
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->Auth->allow('index');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $title = __('Administrator Login');
        $this->viewBuilder()->layout('backend/login');
        $user = TableRegistry::get('Users');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify(); 
            if ($user) { 
                if ($user['user_role_id'] == 1) {   
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard','prefix' => 'admin']);                    
                } else {
                    $this->Flash->error(__('You are not authorized to access.'));
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('user','title'));
    }

    public function logout()
    {
        $this->Auth->logout();
        return $this->redirect('/admin');
    }

    public function dashboard()
    {
        $title = __('Administrator Dashboard');
        $page = __('dashboard');
        $this->viewBuilder()->layout('backend/admin');
    }

    public function userListing() {
        $this->viewBuilder()->layout('backend/admin');

        try {
            if (!isset($this->request->query['search']) ) {
                $this->request->query['search'] = null;
            }
            $usersTable = TableRegistry::get('Users');
            $queryToGetSubAdmins = $usersTable->find()
                                            ->where(function ($exp) {
                                                $orConditions = $exp->or_(['UserProfiles.first_name LIKE' => '%' . $this->request->query['search'] . '%'])
                                                                    ->like('UserProfiles.last_name', '%' . $this->request->query['search'] . '%');
                                                                    
                                                return $exp
                                                    ->add($orConditions)
                                                    ->eq('Users.user_role_id',4);
                                            })
                                            ->contain([
                                                'UserProfiles'
                                            ]);
            $this->paginate = [
                    'sortWhitelist' => [
                        'UserProfiles.first_name', 'UserProfiles.last_name'
                    ],
                    'limit' => 25
                ];
            $users = $this->paginate($queryToGetSubAdmins);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            return $this->redirect(DS . 'admin' . DS . $this->request->params['controller'] . DS . $this->request->params['action'] . '?page=' . $this->request->getParam('paging')['Users']['page']);
        }         
            
      $this->set(compact('users'));
    }

    public function addAdmin() {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_role_id'] = 4;
            $this->request->data['status'] = 1;
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['user_id'] = 1;
            $user = $usersTable->patchEntity($user, $this->request->data, [
                'associated' => ['UserProfiles']
            ]);
            if ($usersTable->save($user)) {
                //$this->__sendSubAdminCreateEmail();
                $this->Flash->success(__('New Sub-Admin has been added successfully.'));
                return $this->redirect($this->referer());
            }
            $errors = $this->_setValidationError($user->errors());
            $this->Flash->error(__('The record could not be added due to the following errors: - ' . $errors));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Some errors occurred. Please try again!'));
        return $this->redirect($this->referer());
    }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editAdmin($id = null)
    {
       $userId = base64_decode($id);
        try {
            if ($id != null) {                
               $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['UserProfiles'])
                   ->first();
            } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('user'));
                $this->render('/Element/Admin/edit_admin');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success('Record not found please try agian');
           return $this->redirect($this->referer());
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['id'] = $userId;
            $this->request->data['user_profile']['id'] = $user['user_profile']->id;
            pr($user);die;
            $user = $this->Users->patchEntity($user, $this->request->getData(),[
                'associated' => ['UserProfiles']
                ]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
