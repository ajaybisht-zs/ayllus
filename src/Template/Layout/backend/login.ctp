<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <?php
            echo $this->Html->css(
                array(
                    'backend/login',
                )
            );
        ?>
        <?= $this->fetch('css') ?>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   </head>
   <body>
    <script type="text/javascript">
      window.url = '<?php echo  $this->Url->build('/',true); ?>';
    </script>
      <?php 
        echo $this->fetch('content');
      ?>
      <?php
        echo $this->Html->script(
                array(
                    'jQuery-2.1.4.min',
                    'bootstrap.min',
                    'jquery.validate',
                    'moment.min',
                    'bootstrap-datetimepicker'
                )
           );
        echo $this->fetch('scriptBottom');
    ?>
   </body>
</html>
