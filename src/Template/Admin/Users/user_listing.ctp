<div class="container-wrapper clearfix">
<section>
	<header><?= __('Manage Subadmins') ?></header>
</section>
<?php echo $this->Flash->render();?>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-left text-left">
			<?php echo $this->Html->link('Add',
				'javascript:void(0)',
				array(
					'class' => 'btn btn-default btn-brown',
					'data-target' => '#addSubadmin',
					'data-toggle' => 'modal'
					)
			); ?>
		</div>
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'subadminForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'Users',
                        'action' => 'userListing'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control ',
				    'type' => 'text',
				    'placeholder' => 'Search by first name or last name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>
			</form>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['First Name', 'Last Name', 'Email', 'Phone','Actions']);
				?>
			</thead>
			<tbody>
				<?php
					if ($users->isEmpty()){
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php } else { ?>
				<?php
					foreach ($users as $key => $value) { 
				?>
						<tr>
							<td>
								<?php echo $value['user_profile']->first_name ?>
							</td>
							<td><?php echo $value['user_profile']->last_name ?></td>
							<td><?php echo $value->email ?></td>
							<td><?php echo $value['user_profile']->phone ?></td>
							<td>
							<?= $this->Html->Link("<span class='fa fa-pencil'></span>",
                                           'javascript:void(0)',
                                           [
                                               'escape'   => false,
                                               'class'    => 'edit-sub-admin btn btn-primary',
                                               'data-url' =>   $this->Url->build(
                                                           [  
                                                               "controller" => "Users",
                                                               "action" => "editAdmin",
                                                               base64_encode($value->id)
                                                           ],true
                                                       ),
                                                'title'  => 'edit'                           
                                           ]
                                       )
                                   ?> 
                            </td>
						</tr>
				<?php } } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
</div>
<?php echo $this->element('Admin/add_admin');?>
<div class="modal Educational-info fade" id="edit-sub-admin" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Subadmins'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script([
            'Admin/add'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>
