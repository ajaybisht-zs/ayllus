<div class="container-wrapper clearfix">
<section>
	<header><?= __('Manage Agents') ?></header>
</section>
<?php echo $this->Flash->render();?>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'subadminForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'Agents',
                        'action' => 'index'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control ',
				    'type' => 'text',
				    'placeholder' => 'Search by first name or last name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>
			</form>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['First Name', 'Last Name', 'Email', 'Company Name','Actions']);
				?>
			</thead>
			<tbody>
				<?php
					if ($users->isEmpty()){
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php } else { ?>
				<?php
					foreach ($users as $key => $value) { 
				?>
						<tr>
							<td>
								<?php echo $value['user_profile']->first_name ?>
							</td>
							<td><?php echo $value['user_profile']->last_name ?></td>
							<td><?php echo $value->email ?></td>
							<td><?php echo $value['user_profile']->company_name ?></td>
							<td>
							<?php  if($value->status == 0) {

								echo $this->Html->Link("<span>Approve</span>",
                                           '/admin/agents/approve/'.base64_encode($value->id),
                                           [
                                               'escape'   => false,
                                               'class'    => 'approve-agent btn btn-primary',
                                                'title'  => 'Approve'                           
                                           ]
                                       );
								echo $this->Html->Link("<span>Disapprove</span>",
                                           '/admin/agents/disapprove/'.base64_encode($value->id),
                                           [
                                               'escape'   => false,
                                               'class'    => 'disapprove-agent btn btn-primary',
                                                'title'  => 'Disapprove'                           
                                           ]
                                       );
								} elseif ($value->status == 1) {
									echo 'Approved';
								} elseif ($value->status == 2) {
									echo 'Disapproved';
								}?> 
                            </td>
						</tr>
				<?php } } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
</div>
<?php echo $this->element('Admin/add_admin');?>
<div class="modal Educational-info fade" id="edit-sub-admin" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Edit Subadmins'); ?></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script([
            'Admin/add'
        ],
        [
            'block' => 'scriptBottom'
    ]);
?>

</section>
</div>