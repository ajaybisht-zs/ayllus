<aside class="sidebar">
    <section>
        <ul class="list-group">
            <li>
                <?php 
                    $active = ($this->request->params['controller'] == 'Users' && $this->request->params['action'] == 'dashboard') ? 'active' : ''; 
                    echo $this->Html->link('<i class="fa fa-clone fa-fw"></i>' . __('Dashboard'),
                        [
                            'controller' => 'Users',
                            'action'    => 'dashboard',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => "list-group-item $active",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = ($this->request->params['controller'] == 'Users' && $this->request->params['action'] == 'userListing') ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-clone fa-fw"></i>' . __('Manage Subadmins'),
                        [
                            'controller' => 'Users',
                            'action'    => 'userListing',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => "list-group-item $active",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = ($this->request->params['controller'] == 'Agents' && $this->request->params['action'] == 'index') ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-clone fa-fw"></i>' . __('Manage Agents'),
                        [
                            'controller' => 'Agents',
                            'action'    => 'index',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => "list-group-item $active",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = ($this->request->params['controller'] == 'CmsPages' && $this->request->params['action'] == 'index') ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-clone fa-fw"></i>' . __('Manage CMS Pages'),
                        [
                            'controller' => 'cms-pages',
                            'action'    => 'index',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => "list-group-item $active",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = ($this->request->params['controller'] == 'EmailTemplates' && $this->request->params['action'] == 'emailTemplateList') ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-paper-plane-o fa-fw"></i>' . __('Manage Email Templates'), [
                            'controller' => 'EmailTemplates',
                            'action' => 'emailTemplateList',
                            'prefix' => 'admin'
                        ], [
                            'class' => "list-group-item $active",
                            'escape' => false
                        ]);
                ?>
            </li>
        </ul>
    </section>
</aside>