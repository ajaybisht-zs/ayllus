<?php
    $this->Form->setTemplates([
    'inputContainer' => '<div>{{content}}</div>'
    ]);
    echo $this->Form->create($user,
            [
                    'url' => [
                        'controller' => 'Users',
                        'action' => 'editAdmin',
                        base64_encode($user->id)
                    ],
                    'id' => 'edit-sub-admin-form',
                    'class' => 'form-horizontal'
            ]
        );
?>            
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('First Name'); ?><span class="required_ast">*</span></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('user_profile.first_name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('First Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Last Name'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('user_profile.last_name',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Last Name'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Phone Number'); ?></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('user_profile.phone',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Phone Number'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Email Address'); ?><span class="required_ast">*</span></label>
        <div class="col-sm-8">
            <?php
                echo $this->Form->input('email',
                        array(
                                'class' => 'form-control',
                                'placeholder' => __('Email Address'),
                                'div' => false,
                                'label' => false
                            )
                    );
            ?>
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <?php
                echo $this->Form->button('Submit', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'submit'
                            ));
            ?>
            <?php
                echo $this->Form->button('Cancel', array(
                            'class' => 'btn submit-info submit_black',
                            'type' => 'button',
                            'data-dismiss' => 'modal'
                            ));
            ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>